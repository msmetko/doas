function [note_type] = getNoteType(segment, line_width)
% Function determines the type of the note
% Input is the segment which contains the note we want to know the type of
% and the width of the line, and the output is the type of the note

coef = 0.64;

% Erode the image
eroded_segment = erode(segment, coef * line_width);

% Determine the number of black pixels in the eroded image
black_mass = eroded_segment == 0;
black_mass = sum(black_mass(:));
% Get the share of black pixels in the whole image
a = 100 * black_mass / numel(eroded_segment);

xprojsize = size(eroded_segment, 1) - sum(eroded_segment, 1);

% Get the share of values in each column of the eroded segment
hist = xprojsize / sum(xprojsize(:));
hist = nonzeros(hist);
b = -sum(hist .* log2(hist));

% Determine the type of the note based on the position of the features
if isMinimOrSemibreve(a, b)
    if a > 2.5
        note_type = "1";
    else
        note_type = "2";
    end
elseif isCrotchet(a, b)
    note_type = "4";
else
    note_type = "8";
end
end

% https://math.stackexchange.com/a/198540
function [is_true] = isCrotchet(x, y)
% Are the values inside the elipse or outside it
is_true = 181*x - 1058*y + 5295 >= 0 && ...
    7615*x + 3875*y - 56032 <= 0 && ...
    664.8*x - 1762*y + 6662 <= 0;
end

function [is_true] = isMinimOrSemibreve(x, y)
% Are the values beneath of above a specified line
is_true = 19*y - 8*x >= 83;
end