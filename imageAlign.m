function img_out = imageAlign(img_in)
% Function imageAlign gets a grayscale image as an input (img_in) and
% returns the binary image (img_out) as a result. The resulting image
% is align so that the music notes are straight.

% Get corner coordinates based on the given image
corners = computeCorners(img_in);

% Get corner coordinates
fixedPoints=[1, 1; 1,size(img_in, 1) - 1;size(img_in, 2) - 1, 1; size(img_in, 2) - 1, size(img_in, 1) - 1];

% Gives the needed projection for imwarp
trans = fitgeotrans(corners, fixedPoints, 'projective');

% Applies the projection on the image
img_out = imwarp(img_in, trans);

% Get new corners so the image can be cropped
corners = computeCorners(img_out);
minX = min(corners(:,1));
minY = min(corners(:,2));
maxY = max(corners(:,2));
maxX =  max(corners(:,1));

% Crop image on gotten corners
img_out = imcrop(img_out, [minX, minY, maxX - minX, maxY - minY]);

img_out = im2bw(img_out, graythresh(img_out));

end

