function [out] = erode(in, width)
% Function erodes the pixels in an image
    se = strel('disk',round(0.6 * width));
    out = imcomplement(imerode(imcomplement(in), se));
end