function [types_and_tones] = main(image_file)
% Main function which starts the note detection and classification
% Input is image file name and output is an array of types and tones

img = imread(image_file);

% If image is rgb, transform it to grayscale
if (size(img, 3) == 3)
    img = rgb2gray(img);
end

% Align image if it's an image taken from an angle
img = imageAlign(img);

% Find staff loaction, line width and gap between the lines
[staff_locations, line_width, lines_gap] = findStaff(img);

% Get each staff as a different segment
segments = segmentStaff(img, staff_locations, lines_gap);
num = 0;
types = [];
tones = string([]);

% Check for each segment individually
for i = 1:length(segments)
    segment = segments{i};
    
    % Fill a segment so that half notes and whole notes are filled
    segment_filled = segment;
    bw = bwlabel(segment_filled);
    rp = regionprops(bw,'FilledArea','PixelIdxList');
    indexesOfHoles = [rp.FilledArea]< 0.75* pi * (lines_gap/2)^2;
    pixelsNotToFill = vertcat(rp(indexesOfHoles).PixelIdxList);
    segment_filled(pixelsNotToFill) = 0;
    
    % Erode image so that the staff lines are lost
    eroded_img = erode(segment_filled, line_width);
    
    % Segment each note individually
    notes = segmentNotes(eroded_img, lines_gap, line_width);
    
    
    % Go over each note for determening the note tone and type
    for j = 1:length(notes)
        
        % Make projections to x and y axis of each segment
        projx = size(eroded_img, 1) - sum(eroded_img(:, notes(j, 1):notes(j, 2)), 1);
        projy = size(eroded_img(:, notes(j, 1):notes(j, 2)), 2) - sum(eroded_img(:, notes(j, 1):notes(j, 2)), 2);
        
        % If the projections contain these values, the segment doesn't
        % contain a note, so skip it
        if(sum(projx(1:40)) > 1750 || sum(projx(1:40)) < 500 || sum(projx(20:end)) < 50 || sum(projy(:) > 0) > 100)
            continue;
        end
        num = num + 1;
        
        % Determine the tone of the note
        [note_tone] = getNoteTone(eroded_img(:, notes(j, 1):notes(j, 2)), segment(:, notes(j, 1):notes(j, 2)), lines_gap);
        
        % Determine the type of the note
        [note_type] = getNoteType(segment(:, notes(j, 1):notes(j,2)), line_width);
        
        % Add the type and tone of the note to fiels
        tones(num) = note_tone;
        types(num) = note_type;
    end
end
types = string(types);
% Get types and tones to the output
types_and_tones = types + tones;
end

function [notePartLocs] = segmentNotes(img, line_gap, line_width)
% Function that segments the notes
% Input is image, gap between lines and line width and the output is the
% starting and ending pixel of the segment on x axis

[height, width] = size(img);
% Make a projection on an x axis
x_proj = height - sum(img, 1);
n = length(x_proj);
notePartLocs = zeros (n,2);
MIN_HEIGHT_SUM_THRESHOLD = 15;
MIN_WIDTH_THRESHOLD = 110;

j=1;
isStart=true;
% Go over each value in the projection on the x axis
for i=1:n
    sumPixel = x_proj(i);
    
    % Looking for the start of the note
    if (isStart && sumPixel > MIN_HEIGHT_SUM_THRESHOLD)
        notePartLocs(j, 1) = i;
        isStart = false;
        % Looking for the end
    elseif (isStart==false && sumPixel <= MIN_HEIGHT_SUM_THRESHOLD && i-notePartLocs(j,1)>=MIN_WIDTH_THRESHOLD)
        notePartLocs(j, 2) = i;
        isStart = true;
        j = j + 1;
    end
end
nonzeros_s1 = length(nonzeros(notePartLocs(:,1)));
nonzeros_s2 = length(nonzeros(notePartLocs(:,2)));
max_nonzeros_s = max(nonzeros_s1,nonzeros_s2);

% Put all the values in the output
notePartLocs = notePartLocs(1:max_nonzeros_s,:);
end
