function [staff_locations, line_width, lines_gap] = findStaff(img)
% Function looks for staff and it's properties
% Input is images and output is staff location (location of each line),
% line width and the width of the gap between the lines

% Make a projection on y axis
projectiony = size(img, 2) - sum(img, 2);

% Find the peaks on the projection. We consider those peaks lines
[pks, staff_locations, wpks] = findpeaks(projectiony, 'SortStr', 'descend', 'MinPeakHeight', 950, 'MinPeakDistance', 23, 'MinPeakWidth', 2, 'MaxPeakWidth', 40);

% Sort staff location by location instead of peak height
staff_locations = sort(staff_locations);

distance_locations = zeros(size(staff_locations, 1) - 1, 1);

% Determine the distance between each two lines in a single staff
for i = 2:length(staff_locations)
    if (mod(i, 5) == 1)
        continue;
    end
    location = staff_locations(i);
    prev_locations = staff_locations(i - 1);
    distance_locations(i - 1, 1) = location - prev_locations;
end

% Line gap width is an average of all the line gap widths
lines_gap = mean(nonzeros(distance_locations));

% Line width is an average of all the peak widths on projection on y
line_width = mean(wpks);
end

