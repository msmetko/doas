function segmented_staff = segmentStaff(img, staff_locations, lines_gap)
% Function that separates staffs based on locations of lines
% Input is image, staff locations and the size of the gap between lines and
% the output is a structure containing individual staff

number_of_segments = round(length(staff_locations) / 5);
staff_segments_locations = ones(number_of_segments, 2);


padding = lines_gap * 2;
j = 0;
% Each 5 lines meka up a segment
for i = 1:5:length(staff_locations)
    % Location of the first line in a segment
    location = staff_locations(i);
    j=j+1;
    
    % If it's the first segment, add it as such, if it's any other segment,
    % i represent the first line of the new segment, and i - 1 represents
    % the last line of the previous segment
    if (i == 1)
        staff_segments_locations(j, 1) = location(i) - padding;
    else
        prev_location = staff_locations(i - 1);
        staff_segments_locations(j - 1, 2) = prev_location + padding;
        staff_segments_locations(j, 1) = location - padding;
    end
    
    % Add the last line as the last line of the last segment
    if (j == number_of_segments)
        staff_segments_locations(j, 2) = staff_locations(length(staff_locations)) + 2 * lines_gap;
    end
end
segmented_staff = {};
n = length(staff_segments_locations);

% Add each segment into a structure that is the output
for i = 1:n
    segmented_staff{i} = img(round(staff_segments_locations(i, 1)):round(staff_segments_locations(i, 2)), :);
end
    
end

