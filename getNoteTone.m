function tone = getNoteTone(note, segment, lines_gap)
% Function returns the note tone
% Input is the eroded segment that contains the note, uneroded segment
% that containt the note and the width of the gap between lines

projectiony = size(segment, 2) - sum(segment, 2);   

% Determine the location of the lines so we have a better precision of
% determening the tone than using global lines
[pks, locs, wpks] = findpeaks(projectiony, 'SortStr', 'descend', 'MinPeakHeight', 90, 'MinPeakDistance', 23, 'MinPeakWidth', 2, 'MaxPeakWidth', 40);

% Sort the locations by locations instead of the peak hight
locs = sort(locs);

% Body of a note is in first 40 pixels on x axis of the image
projy = size(note(:, 1:40), 2) - sum(note(:, 1:40), 2);

gap = lines_gap;

% Set up possible locations of the note and the corresponding tones
pos = [locs(1)-gap/2, locs(1), (locs(1)+locs(2))/2, locs(2), (locs(2)+locs(3))/2, locs(3), (locs(3)+locs(4))/2, locs(4), (locs(4)+locs(5))/2, locs(5), locs(5)+gap/2, locs(5)+gap, locs(5)+3*gap/2];
possibilities = ["G5", "F5", "E5", "D5", "C5", "H4", "A4", "G4", "F4", "E4", "D4", "C4", "H3"];

pospr = pos - gap/3;
pospo = pos + gap/3;
pospo(pospo > size(note, 1)) = size(note, 1);
% Calculate how many pixels are in location of each tone
for i = 1:length(pos)
    trial(i) = sum(projy(round(pospr(i)):round(pospo(i))));
end

% The tone of the note is the one which has the max number of pixels in the
% corresponding location
[m, arg] = max(trial);
tone = possibilities(arg);
end

