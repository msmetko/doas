function corners = computeCorners(img_in)
% Function computeCorners gets a greyscale image (img_in) as an input
% and returns coordinates of the corners (corners) of the given image 
% as a result.

img = img_in;
% Median filter the input image
p_line_size = round(0.01 * size(img, 1));
img = medfilt2(img, [round(1.5 * p_line_size) p_line_size]);

% Turn image into a binary image
img = im2bw(img, graythresh(img));

% Find boundary using 4-connectivity
[B, L] = bwboundaries(img, 4, 'noholes');

% Find out which area is the largest and get the boundary
% pixels for that area
s = regionprops(img, 'Area');
for i = 1:size(B, 1)
    if (i > size(s, 1))
        break;
    end
    S(i) = s(i).Area;
end
[area_max, index_max] = max(S);
B = B{index_max};

% Subtract mean value of each column from values in that column
B_mean_subtracted = bsxfun(@minus, B, mean(B));

% Convert to polar coordinates
[theta, rho] = cart2pol(B_mean_subtracted(:,2), B_mean_subtracted(:,1));

% Find maximum
[pks, corners] = findpeaks(rho, 'SortStr','descend', 'NPeaks', 4, 'MinPeakHeight', 0.5, 'MinPeakDistance', 120);

corners = [B(corners,2), B(corners,1)];
corners = sortrows(corners);
corners = [sortrows(corners(1:2,:),2);sortrows(corners(3:4,:),2)];
end

